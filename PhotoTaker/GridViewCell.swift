//
//  GridViewCell.swift
//  PhotoTaker
//
//  Created by Abubakar on 11/19/15.
//  Copyright © 2015 Tonespy. All rights reserved.
//

import UIKit

class GridViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var livePhotoBadgeImageView: UIImageView!
    var representedAssetIdentifier = NSString()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
//    func setThumbnailImages(thumbnailImage: UIImage) -> Void {
//        //_thumbnailImage = thumbnailImage;
//        self.imageView.image = thumbnailImage
//    }
//    
//    func setLivePhotoBadgeImages(livePhotoBadgeImage: UIImage) -> Void {
//        //_livePhotoBadgeImage = livePhotoBadgeImage;
//        self.livePhotoBadgeImageView.image = livePhotoBadgeImage;
//    }
}
